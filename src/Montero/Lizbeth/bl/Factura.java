package Montero.Lizbeth.bl;

import java.time.LocalDate;
import java.util.ArrayList;

public class Factura {
    private String numero;
    private LocalDate fechaEmision;
    private Cliente cliente;
    private ArrayList<Producto> product;
    private  ArrayList<Linea> lineas;

    /**
     * Constructor por defecto.
     */
    public Factura() {
    }

    /**
     * Constructor que recibe todos los parametros propias de la clase.
     * @param numero tipo String, guarda el numero de la factura
     * @param fechaEmision tipo localDate, guarda el fecha en que se crea la factura.
     */

    public Factura(String numero, LocalDate fechaEmision) {
        this.numero = numero;
        this.fechaEmision = fechaEmision;
    }
    /**
     * Constructor que recibe todos los parametros propias de la clase.
     * @param numero tipo String, guarda el numero de la factura
     * @param fechaEmision tipo localDate, guarda el fecha en que se crea la factura.
     * @param cliente tipo cliente, cliente en al que se le hace una factura.
     *
     */

    public Factura(String numero, LocalDate fechaEmision, Cliente cliente) {
        this.numero = numero;
        this.fechaEmision = fechaEmision;
        this.cliente = cliente;
        this.product= new ArrayList<>();
        this.lineas = new ArrayList<>();
    }
    /**
     * Metodo get de la variable que guarda el el numero de la factura . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la variable que guarda numero de la factura el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public String getNumero() {
        return numero;
    }


    public ArrayList<Linea> getLineas() {
        return lineas;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso digo al numero de linea.
     *
     * @param lineas: variable que almacena  la linea ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setLineas(ArrayList<Linea> lineas) {
        this.lineas = lineas;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso digo al numero de linea.
     *
     * @param numero: variable que almacena  la linea ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }
    /**
     * Metodo get de la variable que guarda el producto que se va a añadir en la factura . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la variable que guarda numero de la factura el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public ArrayList<Producto> getProduct() {
        return product;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso digo al producto.
     *
     * @param product: variable que almacena  el producto ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setProduct(ArrayList<Producto> product) {
        this.product = product;
    }
    /**
     * Metodo get de la variable que guarda el cliente que se va a añadir en la factura . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la variable que guarda el cliente de la factura el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public Cliente getCliente() {
        return cliente;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso digo a la factura
     *
     * @param cliente: variable que almacena  el cliente ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * metodo utilizado para agregar el cliente a la factura.
     * @param nombre tipo string variable que se utiliza para guardar el nombre del cliente.
     * @param cedula tipo string variable que guarda la cedula del cliente.
     * @param genero tipo String varible que guarda el genero del cliente.
     * @param fechaNacimiento tipo localDate guarda la fecha de namiento del cliente.
     * @param edad tipo int  campo calculado que guarda la edad del cliente.
     */

    public void agregarCliente(String nombre, String cedula, String genero, LocalDate fechaNacimiento, int edad) {
        Cliente client = new Cliente(nombre, cedula, genero, fechaNacimiento, edad);
    }
    public void agregarProducto(String codigo, String descripcion, double precio) {
       Producto product = new Producto(codigo,descripcion,precio);
    }

    /**
     * metodo usado para agregar la linea a la factura, donde contiene la informacion del producto.
     * @param numeroLinea tipo int, variable donde se guarda el numero de linea.
     * @param cantidad tipo int cantidad de lineas que se deben agregar.
     * @param producto tipo Producto variable en donde se aguarda la datos del producto registrado en la factura.
     */
    public void agregarLinea(int numeroLinea, double cantidad, Producto producto){
        Linea line = new Linea(numeroLinea,cantidad,producto);
        lineas.add(line);
    }
    /**
     * Metodo get de la variable que guarda  la fecha de emision de la factura  . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve  fecha de emision de la factura  el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public LocalDate getFechaEmision() {
        return fechaEmision;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso digo a la factura
     *
     * @param fechaEmision: variable que almacena  el cliente ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setFechaEmision(LocalDate fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la factura en un solo String.
     */
    @Override
    public String toString() {
        return "Factura{" +
                "numero='" + numero + '\'' +
                ", fechaEmision=" + fechaEmision  +"\n"+
                "----------------------------------"+"\n"+
                " cliente=" + cliente +"\n"+
                "----------------------------------"+"\n"+
                " linea/info=" + lineas +
                '}';
    }
}
