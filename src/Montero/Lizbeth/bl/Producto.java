package Montero.Lizbeth.bl;

/**
 * se definin los valores
 */
public class Producto {
    private String codigo, descripcion;
    private double precio;

    /**
     * contructor por defecto
     */
    public Producto() {
    }

    /**
     * constructor que recibe todos los parametros
     * @param codigo tipo String, se utiliza para idenfificar el producto
     * @param descripcion tipo String, guarda la descripcion del producto
     * @param precio tipo double, guarda el precio de cada producto
     */
    public Producto(String codigo, String descripcion, double precio) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
    }
    /**
     * Metodo get de la variable que guarda el codigo del producto . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve el codigo del producto el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso al  el codigo del producto
     *
     * @param codigo: variable que almacena  el codigo del producto ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    /**
     * Metodo get de la variable que guarda la descripcion del producto . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la descripcion del producto el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public String getDescripcion() {
        return descripcion;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso al  la descripcion del producto
     *
     * @param descripcion: variable que almacena  la descripcion del producto que es ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    /**
     * Metodo get de la variable que guarda el precio del producto . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve el precio del producto el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public double getPrecio() {
        return precio;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso al  precio del producto
     *
     * @param precio: variable que almacena el precio del producto que es ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo del producto en un solo String.
     */
    @Override
    public String toString() {
        return "Producto{" +
                "codigo='" + codigo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", precio=" + precio +
                '}';
    }
}
