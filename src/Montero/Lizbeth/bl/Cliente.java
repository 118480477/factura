package Montero.Lizbeth.bl;

import java.time.LocalDate;

/**
 * se inicializan los atributos
 */
public class Cliente {
    private String nombre, cedula, genero;
    private LocalDate fechaNacimiento;
    private int edad; // campo calculado

    /**
     * constructor por defecto
     */

    public Cliente() {
    }

    /**
     * Constructor que recibe todos los parametros
     * @param nombre tipo String guarda el nombre del cliente
     * @param cedula tipo string guarda la cedula del cliente
     * @param genero rtipo String guarda el genero del cliente
     * @param fechaNacimiento tipo LocalDate guarda la fecha de nacimiento del  cliente.
     * @param edad tipo int, ca,po calculado que guarda el valor de la edad del cliente.
     */
    public Cliente(String nombre, String cedula, String genero, LocalDate fechaNacimiento, int edad) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
    }

    /**
     * Metodo get de la variable que guarda el nombre del cliente . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve el nombre del cliente el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso al  el nombre del cliente
     *
     * @param nombre: variable que el nombre del cliente del producto ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Metodo get de la variable que guarda la cedula del cliente . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la cedula del cliente el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public String getCedula() {
        return cedula;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso al  la cedula del cliente
     *
     * @param cedula: variable que guarda la cedula del cliente del producto ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    /**
     * Metodo get de la variable que guarda el genero del cliente . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve el genero del cliente el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public String getGenero() {
        return genero;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso al  el genero  del cliente
     *
     * @param genero: variable que guarda el genero del cliente del producto ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }
    /**
     * Metodo get de la variable que guarda la fecha de nacimiento  del cliente . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve  la fecha de nacimiento del cliente el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso a la fecha de nacimiento  del cliente
     *
     * @param fechaNacimiento: variable que guarda el genero del cliente del producto ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    /**
     * Metodo get de la variable que guarda la edad  del cliente . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve  la edad del cliente el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public int getEdad() {
        return edad;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso a  la edad  del cliente
     *
     * @param edad: variable que guarda la edad del cliente del producto ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo del cliente en un solo String.
     */

    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", cedula='" + cedula + '\'' +
                ", genero='" + genero + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                '}';
    }
}
