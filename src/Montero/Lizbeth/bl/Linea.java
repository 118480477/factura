package Montero.Lizbeth.bl;

public class Linea {
    private int numeroLinea;
    private double cantidad;
    private Producto producto;

    /**
     * Constructor por defecto
     */
    public Linea() {
    }

    /**
     * Constuctor que recibe todos los parametros
     * @param numeroLinea se almacena el numero de linea, sirve de id, tipo int.
     * @param cantidad se almacena la cantida de lineas que se van a guardar, tipo double, tipo double.
     * @param producto tipo producto,producto a comprar.
     */
    public Linea(int numeroLinea, double cantidad, Producto producto) {
        this.numeroLinea = numeroLinea;
        this.cantidad = cantidad;
        this.producto = producto;
    }
    /**
     * Metodo get de la variable que guarda el el nombrnumero de linea . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la variable que guarda el numero de linea el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */

    public int getNumeroLinea() {
        return numeroLinea;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso digo al numero de linea.
     *
     * @param numeroLinea: variable que almacena el numero de linea ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setNumeroLinea(int numeroLinea) {
        this.numeroLinea = numeroLinea;
    }
    /**
     * Metodo get de la variable que guarda  la cantidad de de lineas . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la variable que guarda la cantidad de de lineas el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public double getCantidad() {
        return cantidad;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso  al numero de linea.
     *
     * @param cantidad: variable que almacena la cantidad de de lineas ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
    /**
     * Metodo get de la variable que guarda  el producto . Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la variable que guarda el producto de la clase el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public Producto getProducto() {
        return producto;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso  al producto.
     *
     * @param producto: variable que almacena el producto que es ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la linea en un solo String.
     */

    @Override
    public String toString() {
        return "Linea/info{" +
                "numeroLinea=" + numeroLinea +
                ", cantidad=" + cantidad +"\n"+
                "----------------------------------"+"\n"+
                " producto=" + producto +
                '}';
    }
}

