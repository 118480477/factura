package Montero.Lizbeth.ui;

import Montero.Lizbeth.bl.*;
import Montero.Lizbeth.tl.Controller;

import java.io.*;
import java.time.LocalDate;

public class Main {


    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller administrador = new Controller();

    public static void main(String[] args) throws IOException {
        crearMenu();
    }

    public static void crearMenu() throws IOException {
        int opcion = 0;
        do {
            out.println("1. Registrar Cliente");
            out.println("2. Listar Clientes");
            out.println("3. Registrar Producto");
            out.println("4. Listar Producto");
            out.println("5. Crear Factura");
            out.println("6. imprimir Facturas");
            out.println("7. Añadir linea a la factura");
            out.println("8. Salir");
            out.print("Digite la opcion: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        } while (opcion != 8);
    }

    public static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion) {
            case 1:
                registrarCliente();
                break;
            case 2:
                imprimirClientes();
                break;
            case 3:
                registrarProducto();
                break;
            case 4:
                imprimirProductos();
                break;
            case 5:
                registrarFactura();
                break;
            case 6:
                imprimirFactura();
                break;
            case 7:
                agregarLineaFactura();
                break;
            case 8:
                System.exit(0);
                out.println("Se ha cerrado correctamente");
                break;
            default:
                out.println("valor registrado");
                break;
        }
    }
    static void mostrarMenu() throws IOException {
        int opcion = -1;
        do {
            System.out.println("Menú");
            System.out.println("1. Registrar cliente");
            System.out.println("2. Listar cliente");
            System.out.println("3. Registrar Producto");
            System.out.println("4. Listar Producto");
            System.out.println("5. Crear factura");
            System.out.println("6. imprimir factura");
            System.out.println("7. Agregar linea a Factura");
            System.out.println("8. Salir");
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);
    }
    static int seleccionarOpcion() throws IOException {
        System.out.println("Digite la opción");
        return Integer.parseInt(in.readLine());

    }

    public static void registrarCliente() throws IOException {
        Cliente client = new Cliente();
        out.println("Ingrese la cedula del cliente");
        String identificacion = in.readLine();
        out.println("Ingrese el nombre del cliente");
        String nombre = in.readLine();
        out.println("Ingrese el genero del cliente, F-Femenino, M-Masculino");
        String genero = in.readLine();
        out.println("Ingrese el dia de nacimiento del cliente");
        int dia = Integer.parseInt(in.readLine());
        out.println("Ingrese el mes nacimiento del cliente");
        int mes = Integer.parseInt(in.readLine());
        out.println("Ingrese el año nacimiento del cliente");
        int year = Integer.parseInt(in.readLine());
        LocalDate fechaNacimiento = LocalDate.of(year, mes, dia);
        int edad = administrador.CalcularEdad(year);
        client.setEdad(edad);
        if (administrador.averiguarCliente(identificacion) == null) {
            String resultado = administrador.registrarCliente(nombre, identificacion, genero, fechaNacimiento, edad);
            out.println(resultado);
        } else {
            out.println("El cliente con cedula: "  + identificacion +  " ya existe en el programa!");
        }

    }

    public static void registrarProducto() throws IOException {
        out.println("Ingrese el codigo del producto");
        String codigo = in.readLine();
        out.println("Ingrese la descripcion del producto");
        String descripcion = in.readLine();
        out.println("Ingrese el precio del producto ");
        Double precio = Double.parseDouble(in.readLine());
        if (administrador.averiguarProducto(codigo) == null) {
            String resultado = administrador.registrarProducto(codigo, descripcion, precio);
            out.println(resultado);
        } else {
            out.println("El producto con el codigo" + codigo + " ya existe en el registro !");
        }
    }

    public static void registrarFactura() throws IOException {
        out.println("Ingrese el numero de factura");
        String numero = in.readLine();
        out.println("Ingrese la cedula del cliente registrado anteriormente");
        String cedula = in.readLine();
        out.println("Ingrese el dia de creacion de la factura");
        int dia = Integer.parseInt(in.readLine());
        out.println("Ingrese el mes  de creacion de la factura");
        int mes = Integer.parseInt(in.readLine());
        out.println("Ingrese el año de creacion de la factura");
        int year = Integer.parseInt(in.readLine());
        LocalDate fechaFactura = LocalDate.of(year, mes, dia);

        if (administrador.averiguarCliente(cedula) != null) {
            if (administrador.averiguarFactura(numero) == null) {
                String resultado = administrador.registrarFactura(numero, cedula, fechaFactura);
                out.println(resultado);
            } else {
                out.println("La factura con numero: " + numero + " no existe!");
            }
        } else {
            out.println("El cliente con cedula: " + cedula + " no existe!");
        }
            out.println("La factura se ha registrado correctamente");
        }


    public static void agregarLineaFactura() throws IOException {
        System.out.print("Ingrese el numero de la factura: ");
        String numero = in.readLine();
        out.println("Ingrese el numero de la linea ");
        int numeroLinea = Integer.parseInt(in.readLine());
        out.println("Ingrese la cantidad de productos ");
        double cantidad = Double.parseDouble(in.readLine());
        System.out.print("Ingrese el codigo del producto ya ingresado : ");
        String codigo = in.readLine();
        if (administrador.averiguarFactura(numero) != null) {
            if (administrador.averiguarProducto(codigo) != null) {
                String respuesta = administrador.agregarLinea(numero, numeroLinea, cantidad, codigo);
                out.println(respuesta);
            } else {
                out.println("El producto codigo" + codigo + " no existe!");
            }
        } else {
            out.println("La factura con numero" + numero + " no existe!");
        }
    }

    public static void imprimirFactura() throws IOException {
        System.out.print("Ingrese el número de la factura a imprimir: ");
        String numeroFactura = in.readLine();
        int posFactura = administrador.buscarPosfactura(numeroFactura);
        String[] datos = administrador.imprimirFactura();
        String datosFactura = datos[posFactura].toString();
        out.println(datosFactura + "\n");

        out.println("------------------------------------");
        out.println("Subtotal: " + administrador.calcularSubTotal(numeroFactura));
        out.println("------------------------------------");
        out.println("Impuestos 13%: " + administrador.calcularImpuesto(numeroFactura));
        out.println("------------------------------------");
        out.println("TOTAL: " + administrador.calcularTotal(numeroFactura));
    }
    public static void imprimirProductos() throws IOException {
        String[] datos = administrador.imprimirProducto();
        for (String producto : datos) {
            out.println(producto);
        }
    }
    public static void imprimirClientes() throws IOException {
        String[] datos = administrador.listarClientes();
        for (String factura : datos) {
            out.println(factura);
        }
    }
}
