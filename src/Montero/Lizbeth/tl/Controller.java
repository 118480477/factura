package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.*;
import Montero.Lizbeth.dl.CapaLogica;


import java.time.LocalDate;

public class Controller {
    private CapaLogica logica = new CapaLogica();

    public String registrarCliente(String cedula, String nombre, String genero, LocalDate fechaNacimiento, int edad) {
        Cliente cliente = new Cliente(cedula, nombre, genero, fechaNacimiento, edad);
        return logica.registrarCliente(cliente);
    }


    public String registrarProducto(String codigo, String descripcion, Double precio) {
        Producto producto = new Producto(codigo, descripcion, precio);
        return logica.registrarProducto(producto);
    }


    public String registrarFactura(String numero, String cedula, LocalDate fechaFactura) {
        Cliente cliente = logica.averiguarCliente(cedula);
        if (cliente != null) {
            Factura factura = new Factura(numero, fechaFactura, cliente);
            return logica.registrarFacturas(factura);
        } else {
            return "El cliente con la cedula : " + cedula + " no existe!";
        }

    }


    public Factura averiguarFactura(String numero) {
        return logica.averiguarFactura(numero);
    }

    public Cliente averiguarCliente(String cedula) {
        return logica.averiguarCliente(cedula);
    }

    public Producto averiguarProducto(String codigo) {
        return logica.averiguarProducto(codigo);
    }


    public String[] listarClientes() {
        return logica.listarClientes();
    }

    public String[] imprimirProducto() {
        return logica.imprimirProducto();
    }

    public String[] imprimirFactura() {
        return logica.imprimirFactura();
    }

    public int CalcularEdad(int year) {
        int edad = 0, actualYear = LocalDate.now().getYear();
        edad = (actualYear - year);
        return edad;
    }

    public String agregarLinea(String numero, int numeroLinea, double cantidad, String codigo) {
        Factura factura = logica.averiguarFactura(numero);
        if (factura != null) {
            Producto prod = logica.averiguarProducto(codigo);
            if (prod != null) {
                factura.agregarLinea(numeroLinea, cantidad, prod);
                return "La linea se agrego exitosamente";
            } else {
                return "El producto con el : " + codigo + " no existe!";
            }
        }
        return "La factura indicada no existe";
    }

    public int buscarPosfactura(String numeroFactura) {
        int encontrada = logica.buscarPosFactura(numeroFactura);
        return encontrada;
    }

    public double calcularImpuesto(String numeroFactura) {
        return logica.calcularSubTotal(numeroFactura) * 13 / 100;
    }

    public double calcularTotal(String numeroFactura) {

        return logica.calcularSubTotal(numeroFactura) + calcularImpuesto(numeroFactura);
    }

    public double calcularSubTotal(String numeroFactura) {
        return logica.calcularSubTotal(numeroFactura);
    }

}
