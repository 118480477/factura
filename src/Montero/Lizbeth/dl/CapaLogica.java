package Montero.Lizbeth.dl;

import Montero.Lizbeth.bl.*;

import java.util.ArrayList;

public class CapaLogica {

    private ArrayList<Producto> productos;
    private ArrayList<Cliente> client;
    private ArrayList<Factura> facturas;
    private ArrayList<Linea> lineas;

    public CapaLogica() {
        lineas = new ArrayList<>();
        productos = new ArrayList<>();
        client = new ArrayList<>();
        facturas = new ArrayList<>();
    }

    public String registrarFacturas(Factura factura) {
        facturas.add(factura);
        return null;
    }

    public String registrarProducto(Producto producto) {
        productos.add(producto);
        return null;
    }

    public String registrarCliente(Cliente clientes) {
        client.add(clientes);
        return null;
    }

    public Factura averiguarFactura(String numero) {
        for (Factura factura : facturas) {
            if (factura.getNumero().equals(numero)) {
                return factura;
            }
        }
        return null;
    }

    public Cliente averiguarCliente(String cedula) {
        for (Cliente cliente : client) {
            if (cedula.equals(cliente.getCedula())) {
                return cliente;
            }
        }
        return null;
    }

    public Producto averiguarProducto(String codigo) {
        for (Producto producto : productos) {
            if (codigo.equals(producto.getCodigo())) {
                return producto;
            }
        }
        return null;
    }

    public String[] listarClientes() {
        String[] info = new String[client.size()];
        for (int i = 0; i < client.size(); i++) {
            info[i] = client.get(i).toString();
        }
        return info;
    }

    public String[] imprimirProducto() {
        String[] info = new String[productos.size()];
        for (int i = 0; i < productos.size(); i++) {
            info[i] = productos.get(i).toString();
        }
        return info;
    }

    public String[] imprimirFactura() {
        String[] info = new String[facturas.size()];
        for (int i = 0; i < facturas.size(); i++) {
            info[i] = facturas.get(i).toString();
        }
        return info;
    }

    public int buscarPosFactura(String numeroFactura) {
        int posicion = 0;
        for (Factura factura : facturas) {
            if (numeroFactura.equals(factura.getNumero())) {
                return posicion;
            }
            posicion++;
        }
        return -1;
    }

    public Double calcularSubTotal(String numeroFactura) {
        Linea linea = new Linea();
        Double subTotal = 0.0;
        ArrayList<Linea> info;
        Linea lineaDetalle;
        info = facturas.get(buscarPosFactura(numeroFactura)).getLineas();
        for (int i = 0; i < info.size(); i++) {
            subTotal += (info.get(i).getCantidad() * info.get(i).getProducto().getPrecio());
        }
        return subTotal;
    }


}// FIN CLASE.


